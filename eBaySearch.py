'''
Created on Jan 15, 2021

@author: User
'''
# First, import the necessary packages:
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import pandas as pd
import time
import re
from PyDevPackage.LazadaSearch import dfS

# Enter the file directory of the Chromedriver
webdriver_path = 'C:/Users/User/AppData/Local/Programs/Python/Python37/Lib/site-packages/chromedriver_win32/chromedriver.exe' 

# Enter the website url
ebay_url = 'https://www.ebay.com.my/'

# # Enter the item to search.
search_item = 'iPhone 11' 

# Open the Chrome browser
browser = webdriver.Chrome(webdriver_path)
browser.get(ebay_url)

time.sleep(2)

# Get the element for 'search bar' by using Chrome Inspect tool. Then press Enter key.
search_bar = browser.find_element(By.XPATH, '//input[@id="gh-ac"]')
search_bar.send_keys(search_item, Keys.ENTER)

time.sleep(2)

# Validate that the result is shown for the mentioned product
SearchResult = browser.find_element(By.XPATH, '//h1[@class ="srp-controls__count-heading"]').text
print(SearchResult)
assert(search_item in SearchResult)

# Get the element for the product name, price and link.(use inspect tool)
item_titles = browser.find_elements_by_xpath('//h3[@class ="s-item__title"]')
item_prices = browser.find_elements_by_xpath('//span[@class ="s-item__price"]')
item_links = browser.find_elements_by_xpath("//a[@class='s-item__link']")

time.sleep(20)

# Use 3 lists, to store the name of products, to store its price and link.
titles_list = []
prices_list = []
links_list = []

time.sleep(20)

# Loop over the item_titles, item_prices and item_links
for title in item_titles:
    titles_list.append(title.text)
    
for price in item_prices:
    prices_list.append(price.text)

for link in item_links:
    links_list.append(link.get_attribute("href"))

time.sleep(10)

# Set printing options to see all column names & rows
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

# Converting the three lists into a dataframe
dfL = pd.DataFrame(zip(prices_list, titles_list, links_list), columns=['Price(RM)', 'Product', 'Link']) 
#dfL['Price(RM)'] = dfL['Price(RM)'].str.split('\o').str[-1].str.strip()

# Do cleanup for the price to convert the entire column into a float type. For sorting the column later.
dfL['Price(RM)'] = dfL['Price(RM)'].str.replace('RM', '').str.replace(',', '').str.replace(' ', '').str.strip()
dfL=dfL[dfL['Price(RM)'].str.contains('to', flags=re.IGNORECASE, regex=True) == False]
dfL['Price(RM)']= dfL['Price(RM)'].astype(float)
dfL['Product'] = dfL['Product'].str.strip()
dfL['Link'] = dfL['Link'].str.strip()

# Add column ['Platform'] for 'eBay' to each of the entries.
# This is for conduct the price comparison between the two website.
dfL['Platform'] = 'eBay'

# Using pandas dataframe, and store it in the "export_dataframe_eBay.csv" file.
dfL.to_csv(r'C:\Users\User\AppData\Local\Programs\Python\Python37\export_dataframe_eBay.csv', index=False, header=True)

# Print the information of the dataframe using the Pandas .info() method to get the Price column type is a string object. 
dfL.info()

# Print the dataframe.
print(dfL)

# Concatenate the Dataframes
df = pd.concat([dfL,dfS])

# Sorting the combination dataframe list of Lazada and eBay website.
df.sort_values(by='Price(RM)', inplace=True)
# df = (df.groupby(['Platform']))
# print the information of the dataframe using the Pandas.
df.info()

# Print the dataframe.
print(df)

# Using pandas dataframe, and store it in the "export_dataframe_lazada.csv" file.
df.to_csv(r'C:\Users\User\AppData\Local\Programs\Python\Python37\export_dataframe_Combine.csv', index = False, header=True)

browser.quit()

