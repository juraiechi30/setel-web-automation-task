Question 2: Web Automation task

1. Open two ecommerce websites say amazon.com and ebay.com (or any other E-Commerce website available at your respective location).
2. Search for “iPhone 11” on the website and validate that the result is shown for the mentioned product
3. Now, combine the outputs of both the websites and display the result in ascending order of price.
4. The output should consist of the following info: Name of the Website, Name of the product, Price of the product and Link to the product.
5. You can print the output in any reporter of your choice or even in the console.

Hi, I'm Juliyati

For this assignment, this is the first time i'm using python to automate.
For the past 14 years, I have experience using Java Language for development during work as application developer for Celcom Web Application
but after that I've move to QA. Since then I've learn manual testing and then move to automation using Test Complete (VB Script) & HP UFT
for Functional, GUI and Web API testing. Most of the experience in automation the script and framework already available and
my role is to execute the script when required for regression test and debug the script.

This assignment, I am using Python script using Selenium, PyDev - Phyton IDE for Eclipse 8.1.0 as plugin, the Chrome web driver to automate 
and Pandas to extract the data and store it in the desired format.

For the two e-commerce, I'll choose eBay and Lazada website.

The details of the code, can be refered to the following file. Each code I have put a comment of the code purpose.
(Can get the file at Left side menu > Download)
a) eBaySearch.Py (Can start run from this fle as this will have th script to combine both result of the ecommerce.)
b) LazadaSearch.Py

The dataframe file ouput can be referred to:
(Can get the file at Left side menu > Download)
a) export_dataframe_Lazada.csv
b) export_dataframe_eBay.csv
c) export_dataframe_Combine.csv

The export_dataframe_Combine output file consist of the data combination of serach result from Lazada and eBay.

There are issue found during sorting the price. The price show for eBay some of it show min to max price.
I have try to split but then when trying to convert to float having issue that list not able to convert to float.
I'm still figure out how to convert or find other solution. So I have to remove it from the list and then convert to float.

The data has been sorted via the script by Price in ascending order.

So i hope that if there is opportunity for me to learn, i'm willing to learn.
Even if i've dont know, I will find the answer.

Thank You.