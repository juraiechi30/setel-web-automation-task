'''
Created on Jan 15, 2021

@author: User
'''
# First, import the necessary packages:

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import pandas as pd
import re

# Enter the file directory of the Chromedriver
webdriver_path = 'C:/Users/User/AppData/Local/Programs/Python/Python37/Lib/site-packages/chromedriver_win32/chromedriver.exe' 

# Enter the website url
Lazada_url = 'https://www.lazada.com.my'

# Enter the item to search.
search_item = 'iPhone 11' 

# Open the Chrome browser
browser = webdriver.Chrome(webdriver_path)
browser.get(Lazada_url)

time.sleep(2)

# Get the element for 'search bar' by using Chrome Inspect tool. Then press Enter key.
search_bar = browser.find_element_by_id('q')
search_bar.send_keys(search_item, Keys.ENTER)

time.sleep(20)

# Validate that the result is shown for the mentioned product
SearchResult = browser.find_element_by_xpath('//div[@class =" c1DXz4"]').text
assert(search_item in SearchResult)
print(SearchResult)

# Get the element for the product name, price and link.(use inspect tool)
item_titles = browser.find_elements_by_class_name('c16H9d')
item_prices = browser.find_elements_by_class_name('c13VH6')
item_links = browser.find_elements_by_xpath("//div[@class='c16H9d']//a[@href]")

time.sleep(20)

# Use 3 lists, to store the name of products, to store its price and link.
titles_list = []
prices_list = []
links_list = []

# Loop over the item_titles, item_prices and item_links
for title in item_titles:
    titles_list.append(title.text)
    
for price in item_prices:
    prices_list.append(price.text)

for link in item_links:
    links_list.append(link.get_attribute("href"))

time.sleep(10)

# Set printing options to see all column names & rows
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

# Converting the three lists into a dataframe
dfS = pd.DataFrame(zip(prices_list, titles_list, links_list), columns=['Price(RM)', 'Product', 'Link']) 

# Do cleanup for the price to convert the entire column into a float type. For sorting the column later.
dfS['Price(RM)'] = dfS['Price(RM)'].str.replace('RM', '').str.replace(',', '').str.replace(' ', '').str.strip()
dfS=dfS[dfS['Price(RM)'].str.contains('to', flags=re.IGNORECASE, regex=True) == False]
dfS['Price(RM)']= dfS['Price(RM)'].astype(float)
dfS['Product'] = dfS['Product'].str.strip()
dfS['Link'] = dfS['Link'].str.strip()

# Create a column 'Platform' and assign 'Lazada' to each of the entries.
# This is for conduct the price comparison between the two website.
dfS['Platform'] = 'Lazada'

# Print the information of the dataframe using the Pandas .info() method to get the Price column type is a string object. 
dfS.info()

# Print the dataframe.
print(dfS)

# Using pandas dataframe, and store it in the "export_dataframe_lazada.csv" file.
dfS.to_csv(r'C:\Users\User\AppData\Local\Programs\Python\Python37\export_dataframe_lazada.csv', index = False, header=True)